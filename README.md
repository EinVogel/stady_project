# Создание виртуального окружения
virtualenv -p /usr/bin/python3.8 venv
# Активация виртуального окружения
source venv/bin/activate
# Подключение зависимостей
pip install -r requirements.txt
# Создание администратора
docker exec -ti django_app python manage.py createsuperuser