FROM python:3

WORKDIR /vogel_site

COPY ./vogel_site /vogel_site
RUN pip install --upgrade pip
RUN pip install -r requirements.txt